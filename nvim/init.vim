lua require('plugins')

let g:config_file_list = [
      \ 'appearance.vim',
      \ 'behaviour.vim',
      \ 'languages.vim',
      \ 'mappings.vim',
      \ ]

for file in g:config_file_list
    execute 'source ' . stdpath('config') . '/' . file
endfor
