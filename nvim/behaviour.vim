""" indentation
set smarttab
set expandtab
set tabstop=8
set shiftwidth=2
set softtabstop=-1
set autoindent
set copyindent
"set shiftround
set cinoptions+=N-s,g0


""" searching
set ignorecase
set smartcase


""" window splits
set splitright
set splitbelow


""" tmux
let g:tmux_navigator_disable_when_zoomed = 1


""" spell
set spelllang=en_gb
autocmd FileType markdown,gitcommit,pandoc setlocal spell
