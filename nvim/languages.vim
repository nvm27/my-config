""" pandoc
"let g:pandoc#formatting#mode = "h"
"let g:pandoc#formatting#mode = "ha"
let g:pandoc#formatting#textwidth = 119
let g:pandoc#formatting#extra_equalprg = "--standalone"
let g:pandoc#after#modules#enabled = [ "ultisnips", "tablemode" ]


""" yaml
autocmd FileType yaml IndentGuidesEnable
autocmd FileType yaml map <leader>f :%!yq e<CR>
