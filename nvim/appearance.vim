""" color schemes
if (has('termguicolors'))
  set termguicolors
endif

let g:sonokai_enable_italic = 1
"let g:sonokai_style = 'atlantis'
colorscheme sonokai

"set background=dark
"let g:one_allow_italics = 1
"colorscheme one

"let g:jellybeans_use_term_italics = 1
"colorscheme jellybeans


""" text rendering
set list
set listchars=trail:·,nbsp:·,tab:»\ ,extends:>,precedes:<

"set linebreak
"set showbreak=>\ 


""" window settings
set number
"set relativenumber
set cursorline

"set foldcolumn=1
set signcolumn=number


""" airline
let g:airline_powerline_fonts = 1
let g:airline#extensions#tabline#enabled = 1
let g:airline#extensions#tabline#show_tabs = 0
let g:airline#extensions#tabline#buffer_idx_mode = 1
