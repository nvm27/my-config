-- bootstrap packer.nvim
local fn = vim.fn
local install_path = fn.stdpath('data')..'/site/pack/packer/start/packer.nvim'

if fn.empty(fn.glob(install_path)) > 0 then
  fn.system({'git', 'clone', '--depth', '1', 'https://github.com/wbthomason/packer.nvim', install_path})
  vim.cmd 'packadd packer.nvim'
end

-- run PackerCompile when changing this file
vim.cmd([[
  augroup packer_user_config
    autocmd!
    autocmd BufWritePost plugins.lua source <afile> | PackerCompile
  augroup end
]])

-- define plugins
return require('packer').startup({
function()
  use 'wbthomason/packer.nvim'

-- # Themes
  use 'sainnhe/sonokai'
  -- use 'rakr/vim-one'
  -- use 'nanotech/jellybeans.vim'


-- # Language support
  use 'euclidianAce/BetterLua.vim'
  use 'vim-pandoc/vim-pandoc'
  use 'vim-pandoc/vim-pandoc-syntax'

  use 'scalameta/nvim-metals'

  use {
    'neovim/nvim-lspconfig',
    config = function()
    end
  }


-- # Tools
  use 'vim-airline/vim-airline'
  use 'christoomey/vim-tmux-navigator'
  use 'tpope/vim-fugitive'
  use 'junegunn/gv.vim'
  use 'nathanaelkane/vim-indent-guides'

  use {
    'ahmedkhalf/project.nvim',
    config = function()
      require('project_nvim').setup{}
    end
  }

  -- install fzf and configure for usage in zsh
  use { 'junegunn/fzf', run = './install --all' }
  --use 'junegunn/fzf.vim'

  use {
    'nvim-telescope/telescope.nvim',
    requires = {
      'nvim-lua/plenary.nvim',
      'nvim-telescope/telescope-fzy-native.nvim',
      'xiyaowong/telescope-emoji.nvim',
   },
    config = function()
      require('telescope').setup{}
      require('telescope').load_extension('fzy_native')
      require('telescope').load_extension('emoji')
    end
  }

  use {
    'sudormrfbin/cheatsheet.nvim',
    requires = {
      'nvim-telescope/telescope.nvim',
      'nvim-lua/popup.nvim',
      'nvim-lua/plenary.nvim',
    }
  }
end,
config = {
  -- 'PackerStatus' command does not work with custom 'compile_path'
  -- compile_path = require('packer.util').join_paths(vim.fn.stdpath('data'), 'site', 'pack', 'packer', 'packer_compiled.lua'),
  display = {
    open_fn = function()
      return require('packer.util').float({ border = 'single' })
    end
  },
}})
