make_cow_think() {
  COW_TYPE=$(\
    find /usr/share/cow*/cows -type f \
    | grep -v -e "telebears" -e "sodomized" -e "head-in" \
    | shuf -n 1\
  )

  WIDTH=$(expr $(stty size | cut -d" " -f2) - 4)

  cowthink -t -W ${WIDTH} -f ${COW_TYPE} "${1}"
}

cowtune() {
  make_cow_think "$(fortune -a)"
}

ctrlp() {
  </dev/tty vim -c CtrlP
}
zle -N ctrlp

bindkey "^p" ctrlp

alias ssh="TERM=xterm-256color ssh"
alias rgt="rg --iglob \!test"

alias glog="git tree"
alias gsu="git submodule update --init --recursive"

alias wow="git status"
alias such="git"
alias very="git"

alias wtf="dmesg"
alias nomnom="killall"
alias yolo="git commit -m '$(curl -s http://whatthecommit.com/index.txt)'"

setopt flowcontrol

unsetopt beep

bindkey "^?" backward-delete-char
bindkey -M viins '\e[3~' vi-delete-char
bindkey -M vicmd '\e[3~' vi-delete-char

# zsh-autosuggestions
bindkey '^ ' autosuggest-accept

eval $(thefuck --alias)

KEYTIMEOUT=1 # no delay for ESC in vi-mode

HISTSIZE=1000
SAVEHIST=1000

export GPG_TTY=$(tty)
gpg-connect-agent updatestartuptty /bye >/dev/null
