""" Neco GHC configuration

set omnifunc=necoghc#omnifunc

let g:necoghc_enable_detailed_browse = 1

let g:haskellmode_completion_ghc = 0
let g:ycm_semantic_triggers = { 'haskell' : ['.'] }
